import React, { Component } from 'react'

class Robot extends Component {
  constructor(props){
    super(props)
    this.state={
      type:this.props.type,
      name:this.props.name,
      mass:this.props.mass
    }
    this.add=()=>{
      this.props.onAdd(this.props.item.id,{
        type:this.props.type,
        name:this.props.name,
        mass:this.props.mass
      })
    }
  }
  render() {
  	let {item} = this.props
    return (
      <div>
  		Hello, my name is {item.name}. I am a {item.type} and weigh {item.mass}
      </div>
    )
  }
}

export default Robot
