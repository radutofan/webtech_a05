import React, { Component } from 'react'
class RobotForm extends Component{
    constructor(props){
        super(props)
        this.state = {
            type : '',
            name  : '',
            mass:''
        }
        this.handleChange = (evt) => {
            this.setState({
                [evt.target.name] : evt.target.value
            })
        }
        
        this.handleClick = () => {
            this.props.onAdd({
                type : this.state.type,
                name : this.state.name,
                mass : this.state.mass
            })
        }
    }

    render(){
        return <div>
            <input type="text" placeholder="name" name="name" id="name" onChange={this.handleChange} />
            <input type="text" placeholder="type" name="type" id="type" onChange={this.handleChange} />
            <input type="text" placeholder="mass" name="mass"  id="mass" onChange={this.handleChange} />
            <input type="button" value="add" onClick={this.handleClick} />  
        </div>
    }
}

export default RobotForm